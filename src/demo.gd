extends "res://src/map.gd"

#Demo level
signal activate_chase1

var checkpoint=0

func _process():
	pass

func _ready():
	next_map="res://test1.tscn"
	self.connect("activate_chase1",chase_walls[0],"_on_activate")


func _on_Player_checkpoint():
	checkpoint+=1
	if(checkpoint==2): emit_signal("activate_chase1")
