extends Area2D


enum direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	NONE
}

const base_speed=20
const max_speed=120

var accel=0
var initial_pos
var grav=direction.NONE
var velocity=Vector2(0,0)
var speed=base_speed
var active=false

var last_grav
var time_since_change=0

func _ready():
	initial_pos=position
	last_grav=grav

func _process(delta):
	
	if active:
		
		if last_grav!=grav:
			time_since_change=0
			speed=base_speed
	
		speed+=time_since_change*accel
				
		if speed>max_speed:
			speed=max_speed
		
		if grav==direction.UP:
			velocity.y=-speed
		elif grav==direction.DOWN:
			velocity.y=speed
		elif grav==direction.LEFT:
			velocity.x=-speed
		elif grav==direction.RIGHT:
			velocity.x=speed
		elif grav==direction.NONE:
			velocity.x=0
			velocity.y=0
			
		time_since_change+=delta
	
	position.x+=velocity.x
	position.y+=velocity.y


func _on_grav_up():
	last_grav=grav
	grav=direction.UP

func _on_grav_down():
	last_grav=grav
	grav=direction.DOWN

func _on_grav_left():
	last_grav=grav
	grav=direction.LEFT

func _on_grav_right():
	last_grav=grav
	grav=direction.RIGHT

func _on_Player_dead():
	last_grav=grav
	position=initial_pos
	grav=direction.NONE

func _on_activate():
	active=true

func set_accel(i):
	accel=i