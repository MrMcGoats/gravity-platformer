extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

signal grav_up
signal grav_down
signal grav_left
signal grav_right
signal grav_stop
signal grav_change
signal checkpoint
signal win

signal dead

enum direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	NONE
}

const base_speed=10
const max_speed=50
const accel=0.5
const collision=true
const slow_rate=60
const rotation_speed = 0.1

var velocity = Vector2(0,0)
var initial_pos=Vector2(790,540)
var last_pos=initial_pos

var speed=base_speed

var last_grav
var grav
var time_since_change=0

var sprite
var camera

var difficulty=false #False==easy, true==hard

var rotater=""

var full_zoom=5
var target_zoom=5 #When we zoom in or out, this is how we save the min/max zoom
var zoom_speed=10 #How fast to zoom in and out
var zoom_at_checkpoint=[]


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	initial_pos=position
	grav=direction.NONE
	last_grav=grav
	sprite=get_node("Sprite")
	camera=get_node("Camera2D")
	
	sprite.rotation=PI
	
	sprite.animation="start"
	
	rotater=sprite
	camera.zoom=Vector2(5,5)
	
	
	
	if difficulty:
		
		camera.rotating=true
		rotater=camera
	
	
	
	
func get_input():
	last_grav=grav
	#grav=direction.LEFT
	#return 0
	if Input.is_action_just_pressed('ui_right'):
		grav=direction.RIGHT
		if last_grav!=grav:
			emit_signal("grav_right")
	elif Input.is_action_just_pressed('ui_left'):
		grav=direction.LEFT
		if last_grav!=grav:
			emit_signal("grav_left")
	elif Input.is_action_just_pressed('ui_up'):
		grav=direction.UP
		if last_grav!=grav:
			emit_signal("grav_up")
	elif Input.is_action_just_pressed('ui_down'):
		grav=direction.DOWN
		if last_grav!=grav:
			emit_signal("grav_down")
			
	if grav!=last_grav:
		emit_signal("grav_change")

func _physics_process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	get_input()
	#print("x:"+str(velocity.x)+"y:"+str(velocity.y))
	var overshoot=full_zoom-target_zoom
	var last_overshoot=overshoot
	
	#if overshoot!=last_overshoot:
	#	full_zoom=target_zoom
	
	overshoot=1
	if overshoot!=0:
		if full_zoom>target_zoom:
			full_zoom-=delta*zoom_speed
		elif full_zoom<target_zoom:
			full_zoom+=delta*zoom_speed
	
	overshoot=full_zoom-target_zoom
	if (overshoot>0 and last_overshoot<=0) or (overshoot<0 and last_overshoot>=0):
		full_zoom=target_zoom
	
	camera.zoom=Vector2(full_zoom,full_zoom)
	var slow_speed=slow_rate*delta
	
	if last_grav!=grav:
		time_since_change=0
		speed=base_speed
		
		if last_grav==direction.NONE:
			sprite.animation="start"
			sprite.play("start")
	
	
	speed+=time_since_change*accel
		
	if speed>max_speed:
		speed=max_speed
	
	
	if grav==direction.RIGHT:
		if velocity.x<speed:
			velocity.x+=slow_speed
		else:
			velocity.x=speed
			
		if velocity.y>0:
			velocity.y-=slow_speed
		else:
			velocity.y=0
			
		#detect and compensate -- 5% band
		if 	rotater.rotation<-0.95*PI/2 && rotater.rotation>-1.05*PI/2:
			pass
		elif rotater.rotation<-0.95*PI/2:
			rotater.rotation+=rotation_speed
		elif rotater.rotation>-1.05*PI/2:
			rotater.rotation-=rotation_speed
		
	elif grav==direction.LEFT:
		if velocity.x>-speed:
			velocity.x-=slow_speed
		else:
			velocity.x=-speed
		
		if velocity.y>0:
			velocity.y-=slow_speed
		else:
			velocity.y=0
		
		if 	rotater.rotation<0.95*PI/2 && rotater.rotation>1.05*PI/2:
			pass
		elif rotater.rotation<0.95*PI/2:
			rotater.rotation+=rotation_speed
		elif rotater.rotation>1.05*PI/2:
			rotater.rotation-=rotation_speed
		
	elif grav==direction.UP:
		if velocity.y>-speed:
			velocity.y-=slow_speed
		else:
			velocity.y=-speed
		
		if velocity.x>0:
			velocity.x-=slow_speed
		else:
			velocity.x=0
		
		if 	rotater.rotation<0.95*PI && rotater.rotation>1.05*PI:
			pass
		elif rotater.rotation<0.95*PI:
			rotater.rotation+=rotation_speed
		elif rotater.rotation>1.05*PI:
			rotater.rotation-=rotation_speed
			
	elif grav==direction.DOWN:
		if velocity.y<speed:
			velocity.y+=slow_speed
		else:
			velocity.y=speed
			
		if velocity.x>0:
			velocity.x-=slow_speed
		else:
			velocity.x=0
		
		if 	rotater.rotation<-0.95*PI && rotater.rotation>-1.05*PI:
			pass
		elif rotater.rotation<-0.95*PI:
			rotater.rotation+=rotation_speed
		elif rotater.rotation>-1.05*PI:
			rotater.rotation-=rotation_speed
			
	elif grav==direction.NONE:
		speed=0
		velocity.x=0
		velocity.y=0
		sprite.rotation=0
		sprite.stop()
		sprite.animation="start"
		
		
	move_and_collide(velocity)
	#print("("+str(position.x)+","+str(position.y)+") rot:"+str(sprite.rotation))
	
	time_since_change+=delta
	
	if sprite.animation=="start" and sprite.frame==6 and grav!=direction.NONE:
		sprite.animation="default"
		sprite.play("default")

	
func kill():
	emit_signal("dead")
	emit_signal("grav_stop")
	grav=direction.NONE
	position=initial_pos
	sprite.rotation=0


func _on_Floor_body_entered(body=null):
	
	if(body!=null): body.kill()
	else: kill()

func _on_Goal_body_entered(body=null,next=null):
	emit_signal("win",next)
	grav=direction.NONE


func _on_Checkpoint_body_entered(body):
	emit_signal("checkpoint")
	initial_pos=position


func is_class(c):
	return c=="player" || c=="Player" || c=="res://src/src/player.gd"

func change_diff(diff):
	difficulty=diff

func _on_zoom_trigger(body, target, speed=10):
	target_zoom=target
	zoom_speed=speed