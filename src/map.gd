extends Node2D

var map
var player
var tiles

var chase_walls=[]
var chase_walls_activate=[] #number of checkpoints to pass before the moving platform is activated

var checkpoints=0 #number of checkpoints passed
var collected = 0#number of collectibles collected

func _ready():
	player=get_node("Player")
	map=get_node("map/Object Layer 1")
	tiles=get_node("map/Tile Layer 1")
	for i in map.get_children():
		if i.is_class("Area2D"):
			if i.get_meta("Floor"):
				i.connect("body_entered",player,"_on_Floor_body_entered")
			elif i.get_meta("Checkpoint"):
				i.connect("body_entered",player,"_on_Checkpoint_body_entered",[],CONNECT_ONESHOT)
			elif i.get_meta("Goal"):
				i.connect("body_entered",player,"_on_Goal_body_entered",[i.get_meta("next_map")],CONNECT_ONESHOT)
			elif i.get_meta("Zoom"):
				i.connect("body_entered",player,"_on_zoom_trigger",[i.get_meta("target"),i.get_meta("speed")],CONNECT_ONESHOT)

	
	for i in get_node("map").get_children():
		if i.get_meta("moving"):
			chase_walls.append(i)
			chase_walls_activate.append(i.get_meta("checkpoints"))
			add_user_signal("chase_wall_activate_"+str(len(chase_walls)-1))
			self.connect("chase_wall_activate_"+str(len(chase_walls)-1),i,"_on_activate")
			if i.get_meta("chase wall"):
				i.set_script(load("res://src/chase wall.gd"))
				i._ready()
				i.set_accel(player.accel)
				player.connect("grav_up",i,"_on_grav_right")
				player.connect("grav_down",i,"_on_grav_left")
				player.connect("grav_left",i,"_on_grav_left")
				player.connect("grav_right",i,"_on_grav_right")
				player.connect("dead",i,"_on_Player_dead")
				player.connect("grav_stop",i,"_on_Player_dead")
				for j in i.get_children():
					if j.is_class("Node2D"):
						for k in j.get_children():
							if k.is_class("Area2D"):
								k.connect("body_entered",player,"_on_Floor_body_entered")
								
			elif i.get_meta("fall platform"):
				i.set_script(load("res://src/fall_platform.gd"))
				i._ready()
				i.set_accel(player.accel)
				player.connect("grav_up",i,"_on_grav_up")
				player.connect("grav_down",i,"_on_grav_down")
				player.connect("grav_left",i,"_on_grav_down")
				player.connect("grav_right",i,"_on_grav_up")
				player.connect("dead",i,"_on_Player_dead")
				player.connect("grav_stop",i,"_on_Player_dead")
				for j in i.get_children():
					if j.is_class("Node2D"):
						for k in j.get_children():
							if k.is_class("Area2D"):
								k.connect("body_entered",player,"_on_Floor_body_entered")
								
			elif i.get_meta("fourway platform"):
				i.set_script(load("res://src/fourway_platform.gd"))
				i._ready()
				i.set_accel(player.accel)
				player.connect("grav_up",i,"_on_grav_up")
				player.connect("grav_down",i,"_on_grav_down")
				player.connect("grav_left",i,"_on_grav_left")
				player.connect("grav_right",i,"_on_grav_right")
				player.connect("dead",i,"_on_Player_dead")
				player.connect("grav_stop",i,"_on_Player_dead")
				for j in i.get_children():
					if j.is_class("Node2D"):
						for k in j.get_children():
							if k.is_class("Area2D"):
								k.connect("body_entered",player,"_on_Floor_body_entered")
								
		elif i.get_meta("collectible"):
			#print("found1")
			for j in i.get_children():
				if j.is_class("Node2D"):
					#print("found2")
					for k in j.get_children():
						if k.is_class("Area2D"):
							var score=1
							if i.get_meta("score"):
								score=i.get_meta("score")
							k.connect("body_entered",self,"_on_collected",[i,score],CONNECT_ONESHOT)
							#print("added")
								
		
				
				
	player.connect("win",self,"_on_Player_win")
	player.connect("win",get_node("Player/Camera2D/RichTextLabel"),"_on_Player_win")
	player.connect("checkpoint",self,"_on_Player_checkpoint")

func _process(delta):
	for i in chase_walls:
		i._process(delta)

func _on_Player_win(next):
	#print(next)
	OS.delay_msec(1000)
	get_tree().change_scene(next)

func _on_Player_checkpoint():
	checkpoints+=1
	for i in range(len(chase_walls)):
		if chase_walls_activate[i]==checkpoints:
			emit_signal("chase_wall_activate_"+str(i))

func _on_collected(body, collectible, score):
	collected += score
	collectible.visible=false
	print("fuck")
	$CanvasLayer/ScoreDisplay.text = "Score = %s" %collected
	
func is_class(c):
	return c=="map" || c=="res://src/map.gd"