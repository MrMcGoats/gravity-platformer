extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var aspect_ratio=get_viewport_rect().size.x/get_viewport_rect().size.y

func _ready():
	on_resize()
	get_tree().get_root().connect("size_changed",self,"on_resize")

func on_resize():
	var room_size=Vector2(1000,1000)
	room_size.y=room_size.x/aspect_ratio
	var zoom=Vector2(room_size.x/get_viewport_rect().size.x,room_size.y/get_viewport_rect().size.y)
	set_zoom(zoom)